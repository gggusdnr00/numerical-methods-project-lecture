# 수치해석 프로젝트
   - Computational Experiments for Aerospace Engineering
   - ASE3001

## 강의 목표
   - 항공우주공학 분야 문제 해결을 위한 수치적 해석 기법 학습
   - 프로그래밍 언어로 공학 문제를 표현, 해석 및 결과 능력 배양
   - 항공우주 시스템 시뮬레이션 + 최신 머신러닝 기법 활용

## 선수과목
   - 컴퓨터 프로그래밍, 수치해석

## 유의사항
   - 프로그래밍은 Python으로 진행됩니다. 기본적인 Python 사용법은 1주차에 소개하지만, [Scipy Lecture Note](https://scipy-lectures.org/) 참고 바랍니다.
      * [Python 특징](https://colab.research.google.com/drive/1USw1ZzyQlrXJatvfv_kPiyyhj8uYUVmO)
      * [Python 문법](https://colab.research.google.com/drive/1n-JBTkq5wCD_mEHI4qDJ1-tXXoRrTDAe)
      * [Numpy 소개](https://colab.research.google.com/drive/16_90Pz5b4-MI0ttUxTCkQMcRJBiR2L4N)
   - 매 수업은 강의 후 실습을 진행합니다.

## 주차별 강의 내용 
   - 1주차 - 수치해석 기본 이론 소개
   - 2주차 - Python/Pandas를 이용한 데이터 처리 및 가시화
   - 3주차 - Root Finding: 경사충격파 해석 및 흡입구 설계
   - 4주차 - Fourier 변환: 음향 신호 처리
   - 5주차 - 선형 방정식 해석: Spring-Mass System 해석
   - 6주차 - 통계 처리: 주가 데이터 분석 및 수익률 예측
   - 7주차 - 상미분 방정식 해석: 위성 궤도 해석
   - 8주차 - 중간고사
   - 9주차 - 상미분 방정식 해석: Stiff Problem & 경계층 유동 해석
   - 10주차 - 제어: 재사용로켓 착륙 궤적 설계
   - 11주차 - 편미분 방정식: 1-D Heat Equation 해석
   - 12주차 - 편미분 방정식: Laplace Equation 해석
   - 13주차 - 병렬 프로그래밍 및 계산 가속화 개요
   - 14주차 - 머신러닝: 비행체 자세 데이터 예측
   - 15주차 - 기말고사
